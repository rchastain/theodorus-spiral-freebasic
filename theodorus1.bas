
' theodorus.bas

#include "vectors.bi"

const SW = 640
const SH = SW
const CNumTriangles = 16
const CScale = 72

dim shared LCenter as TVector = (SW / 2, SH / 2)
dim shared LColor as uinteger = rgb(0, 255, 0)

sub DrawLine(byval APoint1 as TVector, byval APoint2 as TVector)
  
  APoint1 = Add(LCenter, Mirror(Scale(APoint1, CScale)))
  APoint2 = Add(LCenter, Mirror(Scale(APoint2, CScale)))
  
  line(APoint1.x, APoint1.y)-(APoint2.x, APoint2.y), LColor
end sub

screenres SW, SH, 32
windowtitle "Theodorus Spiral"

dim A as TVector = (0.0, 0.0)
dim B as TVector = (1.0, 0.0)
dim C as TVector

for i as integer = 1 to CNumTriangles
  C = Add(B, Normalize(Perpendicular(B)))
  
  DrawLine(A, B)
  DrawLine(B, C)

  B = C
next

' Close the last triangle
DrawLine(C, A)

sleep
