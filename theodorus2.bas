
' Theodorus spiral

#include "vectors.bi"

const SW = 640
const SH = SW
const CNumTriangles = 16
const CScale = 72

dim shared LCenter as TVector = (SW / 2, SH / 2)
dim shared LColor as uinteger = rgb(0, 255, 0)

sub DrawTriangle(byval APoint1 as TVector, byval APoint2 as TVector, byval APoint3 as TVector)
  
  ' Convert points to screen coordinates
  APoint1 = Add(LCenter, Mirror(Scale(APoint1, CScale)))
  APoint2 = Add(LCenter, Mirror(Scale(APoint2, CScale)))
  APoint3 = Add(LCenter, Mirror(Scale(APoint3, CScale)))
  
  line(APoint1.x, APoint1.y)-(APoint2.x, APoint2.y), LColor
  line(APoint2.x, APoint2.y)-(APoint3.x, APoint3.y), LColor
  line(APoint3.x, APoint3.y)-(APoint1.x, APoint1.y), LColor
end sub

screenres SW, SH, 32
windowtitle "Theodorus Spiral"

dim LPoint1 as TVector = (0.0, 0.0)
dim LPoint2 as TVector = (1.0, 0.0)
dim LPoint3 as TVector

for i as integer = 1 to CNumTriangles
  LPoint3 = Add(LPoint2, Normalize(Perpendicular(LPoint2)))
  
  DrawTriangle(LPoint1, LPoint2, LPoint3)

  LPoint2 = LPoint3
next

sleep
