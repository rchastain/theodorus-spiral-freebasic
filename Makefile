SOURCES := $(wildcard *.bas)
TARGETS := $(SOURCES:%.bas=%)

LEMONCHIFFON=FFFACDFF
LIGHTYELLOW=FFFFE0FF
ORANGE=FFA500FF

all: $(TARGETS)

%: %.bas
	fbc $<

clean:
	rm -f theodorus?

test: theodorus3
	./$< --bkcolor=$(LEMONCHIFFON) --pencolor=$(ORANGE) --fillcolor=$(LIGHTYELLOW) --triangles=16
