' color.bi

type TColor
  r as double
  g as double
  b as double
  a as double
  declare constructor(as uinteger)
  declare constructor(as double, as double, as double, as double = 1.0)
end type

constructor TColor(rgba_ as uinteger)
  r = (rgba_ and &hFF000000) / &hFF000000
  g = (rgba_ and &h00FF0000) / &h00FF0000
  b = (rgba_ and &h0000FF00) / &h0000FF00
  a = (rgba_ and &h000000FF) / &h000000FF
end constructor

constructor TColor(r_ as double, g_ as double, b_ as double, a_ as double)
  r = r_
  g = g_
  b = b_
  a = a_
end constructor

/'
dim c1 as TColor = TColor(&hFF00FF80)

with c1
  print(.r)
  print(.g)
  print(.b)
  print(.a)
end with

dim c2 as TColor = TColor(1.0, 1.0, 1.0, 0.5)

with c2
  print(.r)
  print(.g)
  print(.b)
  print(.a)
end with
'/
