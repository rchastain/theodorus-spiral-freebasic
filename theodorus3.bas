
' Theodorus spiral

#include "cairo/cairo.bi"
#include "color.bi"
#include "command.bi"
#include "vectors.bi"

#define EXPAND_TCOLOR(c) c.r, c.g, c.b, c.a

dim as integer scrWidth = 640
dim as integer scrHeight = scrWidth
dim as integer numTriangles = 16
dim shared as double scale_ = 64

sub DrawTriangle(byval APoint1 as TVector, byval APoint2 as TVector, byval APoint3 as TVector, byval cr as cairo_t ptr, pen as TColor, fill as TColor)
  
  dim as TVector LPoint4, LPoint5, LPoint6
  
  APoint1 = Scale(APoint1, scale_)
  APoint2 = Scale(APoint2, scale_)
  APoint3 = Scale(APoint3, scale_)

  cairo_move_to(cr, APoint1.x, APoint1.y)
  cairo_line_to(cr, APoint2.x, APoint2.y)
  cairo_line_to(cr, APoint3.x, APoint3.y)
  cairo_close_path(cr)
  
  cairo_set_source_rgba(cr, EXPAND_TCOLOR(fill))
  cairo_fill_preserve(cr)
  cairo_set_source_rgba(cr, EXPAND_TCOLOR(pen))
  cairo_stroke(cr)
  
  LPoint4 = Scale(Normalize(Perpendicular(APoint2)), 8)
  LPoint5 = Perpendicular(LPoint4)
  LPoint6 = Perpendicular(LPoint5)
  
  LPoint4 = Add(APoint2, LPoint4)
  
  cairo_move_to(cr, LPoint4.x, LPoint4.y)
  cairo_rel_line_to(cr, LPoint5.x, LPoint5.y)
  cairo_rel_line_to(cr, LPoint6.x, LPoint6.y)
  cairo_set_source_rgba(cr, EXPAND_TCOLOR(pen))
  cairo_stroke(cr)
end sub

screenres scrWidth, scrHeight, 32
windowtitle "Theodorus Spiral"

dim cl as TCommandLine

dim as TColor bkColor = TColor(0.0, 0.0, 0.0, 0.0)
dim as TColor penColor = TColor(0.0, 0.3, 0.0, 1.0)
dim as TColor fillColor = TColor(0.0, 0.5, 0.0, 1.0)

if cl.HasOption("bkcolor") then
  bkColor = TColor(valint("&h" & cl.GetOptionValue("bkcolor")))
end if
if cl.HasOption("pencolor") then
  penColor = TColor(valint("&h" & cl.GetOptionValue("pencolor")))
end if
if cl.HasOption("fillcolor") then
  fillColor = TColor(valint("&h" & cl.GetOptionValue("fillcolor")))
end if
if cl.HasOption("triangles") then
  numTriangles = valint(cl.GetOptionValue("triangles"))
end if

dim as any ptr image = imagecreate(scrWidth, scrHeight, rgba(0, 0, 0, 0))
dim as any ptr pixels
imageinfo(image, scrWidth, scrHeight,,, pixels)
dim as cairo_surface_t ptr sf = cairo_image_surface_create_for_data(pixels, CAIRO_FORMAT_ARGB32, scrWidth, scrHeight, scrWidth * 4)
dim as cairo_t ptr cr = cairo_create(sf)

cairo_set_source_rgba(cr, EXPAND_TCOLOR(bkColor))
cairo_paint(cr)
cairo_translate(cr, scrWidth / 2, scrHeight / 2)
cairo_scale(cr, 1, -1)
cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND)
cairo_set_line_width(cr, 0.8)

dim LPoint1 as TVector = (0.0, 0.0)
dim LPoint2 as TVector = (1.0, 0.0)
dim LPoint3 as TVector

for i as integer = 1 to numTriangles
  LPoint3 = Add(LPoint2, Normalize(Perpendicular(LPoint2)))

  DrawTriangle(LPoint1, LPoint2, LPoint3, cr, penColor, fillColor)

  LPoint2 = LPoint3
next

cairo_destroy(cr)
cairo_surface_write_to_png(sf, "image.png")
cairo_surface_destroy(sf)

screenlock()
put (0, 0), image, pset
screenunlock()

imagedestroy image

sleep
