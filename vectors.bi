
' vectors.bi

type TVector
  x as double
  y as double
end type

declare function Rotate(byval AVector as TVector, byval ATheta as double) as TVector
declare function Perpendicular(byval AVector as TVector, byval AClockWise as boolean = FALSE) as TVector
declare function Normalize(byval AVector as TVector) as TVector
declare function Add(byval AVector1 as TVector, byval AVector2 as TVector) as TVector
declare function Scale(byval AVector as TVector, byval AFactor as double) as TVector
declare function Mirror(byval AVector as TVector) as TVector

function Rotate(byval AVector as TVector, byval ATheta as double) as TVector
/'
  x' = x cos θ − y sin θ
  y' = x sin θ + y cos θ
'/
  dim result as TVector
  with AVector
    result.x = .x * Cos(ATheta) - .y * Sin(ATheta)
    result.y = .x * Sin(ATheta) + .y * Cos(ATheta)
  end with
  return result
end function

function Perpendicular(byval AVector as TVector, byval AClockWise as boolean) as TVector
/'
  x' = x cos θ − y sin θ
  y' = x sin θ + y cos θ
'/
  dim result as TVector
  dim as integer LSign = IIf(AClockWise, -1, 1)
  with AVector
    result.x = -.y * LSign
    result.y =  .x * LSign
  end with
  return result
end function

function Normalize(byval AVector as TVector) as TVector
  dim result as TVector
  dim LLength as double
  with AVector
    LLength = Sqr(.x * .x + .y * .y)
    result.x = .x / LLength
    result.y = .y / LLength
  end with
  return result
end function

function Add(byval AVector1 as TVector, byval AVector2 as TVector) as TVector
  dim result as TVector
  result.x = AVector1.x + AVector2.x
  result.y = AVector1.y + AVector2.y
  return result
end function

function Scale(byval AVector as TVector, byval AFactor as double) as TVector
  dim result as TVector
  with AVector
    result.x = .x * AFactor
    result.y = .y * AFactor
  end with
  return result
end function

function Mirror(byval AVector as TVector) as TVector
  dim result as TVector
  with AVector
    result.x =  .x
    result.y = -.y
  end with
  return result
end function
