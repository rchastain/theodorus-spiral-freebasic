
' https://www.freebasic.net/forum/viewtopic.php?p=206588#p206588

'  Tested with the passed parameters:
'  "+3 --option1 -4 --option2=-5 foo.bas --option3=file.ext"

type TCommandLineOption
  public:
    key as string ' option name
    value as string
end type

type TCommandLine
  public:
    declare constructor ()
    declare function HasOption(byref aOptionName as const string) as integer
    declare function GetOptionValue(byref aOptionName as const string) as string
    declare function GetAloneValueNth(byval n as integer) as string
  private:
    dim aOptions(any) as TCommandLineOption
End Type

function SameString(byref aLeft as const string, byref aRight as const string, byref aCaseSensitive as const boolean) as boolean
  if aCaseSensitive then
    return aLeft = aRight
  else
    return lcase(aLeft) = lcase(aRight)
  end if
end function

dim shared caseSensitive as boolean = false

constructor TCommandLine ()
  erase(aOptions)
  dim i as integer = 0
  do while command(i + 1) <> ""
    redim preserve aOptions(i)
    if left(command(i + 1), 2) = "--" then
      dim n as integer = instr(3, command(i + 1), "=")
      if n = 0 then
        aOptions(i).key = mid(command(i + 1), 3)
      else
        aOptions(i).key = mid(command(i + 1), 3, n - 3)
        aOptions(i).value = mid(command(i + 1), n + 1)
      end if
    else
      aOptions(i).value = command(i + 1)
    end if
    i += 1
  loop
end constructor

function TCommandLine.HasOption(byref aOptionName as const string) as integer
  for i as integer = LBound(aOptions) to UBound(aOptions)
    if (aOptions(i).key <> "") andalso SameString(aOptions(i).key, aOptionName, caseSensitive) then
      return -1
    end if
  next i
  return 0
end function

function TCommandLine.GetOptionValue(byref aOptionName as const string) as string
  for i as integer = LBound(aOptions) to UBound(aOptions)
    if (aOptions(i).key <> "") andalso SameString(aOptions(i).key, aOptionName, caseSensitive) then
      return aOptions(i).value
    end if
  next i
  return ""
end function

function TCommandLine.GetAloneValueNth(byval n as integer) as string
  dim k as integer = 0
  for i as integer = LBound(aOptions) to UBound(aOptions)
    if aOptions(i).key = "" then
      k += 1
      if k = n then
        return aOptions(i).value
      end if
    end if 
  next i
  return ""
end function

/'
dim myOptions as TCommandLine

print(myOptions.HasOption("option1"))
print("[" & myOptions.GetOptionValue("option2") & "]")

dim n as integer = 1
var s = myOptions.GetAloneValueNth(n)
do while s <> ""
  print("[" & s & "]")
  n += 1
  s = myOptions.GetAloneValueNth(n)
loop
'/
